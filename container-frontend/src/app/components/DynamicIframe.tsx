"use client"
import React, { useEffect, useRef } from "react";

interface DynamicIframeProps {
  src: string;
  width?: string;
  height?: string;
  sandbox?: string;
}

const DynamicIframe: React.FC<DynamicIframeProps> = ({
  src,
  width = "100%",
  height = "100%",
  sandbox = "allow-scripts",
}) => {
  const iframeRef = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    const handleMessage = (event: MessageEvent) => {
      if (event.data && event.data.type === "SET_HEIGHT") {
        const height = event.data.height;
        iframeRef.current!.style.height = `${height}px`;
        iframeRef.current!.style.minHeight = `${window.innerHeight - 1}px`;
      }
    };

    window.addEventListener("message", handleMessage);

    return () => {
      window.removeEventListener("message", handleMessage);
    };
  }, []);

  return (
    <div>
      <iframe
        ref={iframeRef}
        src={src}
        width={width}
        height={height}
        sandbox={sandbox}
      ></iframe>
    </div>
  );
};

export default DynamicIframe;

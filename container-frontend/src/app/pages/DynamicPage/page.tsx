"use client"
import React, { useEffect, useState } from "react";
import DynamicIframe from "../../components/DynamicIframe";
import * as routes from "../../assets/mocks/menu.json";

interface Routes {
  [key: string]: string;
}

const HomePage: React.FC = () => {
  const [destination, setDestination] = useState<string>("");

  useEffect(() => {
    const currentPath = window.location.pathname;
    const typedRoutes: Routes = routes as Routes;
    const destination = typedRoutes[currentPath] || "";
    setDestination(destination);
  }, []);

  return (
    <div>
      <DynamicIframe src={destination} />
    </div>
  );
};

export default HomePage;

const nextConfig = {
  pageExtensions: ["js", "jsx", "ts", "tsx"],

  webpack(config) {
    config.resolve.modules.push(__dirname);
    config.resolve.modules.push("./src");
    return config;
  },

  async headers() {
    return [
      {
        source: "/(.*)",
        headers: [
          {
            key: "Access-Control-Allow-Origin",
            value: "http://localhost:4201",
          },
          {
            key: "Access-Control-Allow-Methods",
            value: "GET, POST, PUT, DELETE",
          },
          {
            key: "Access-Control-Allow-Headers",
            value: "Content-Type",
          },
        ],
      },
    ];
  },

  async rewrites() {
    return [
      {
        source: "/pagina-no-encontrada", 
        destination: "/pages/NotFoundPage", 
      },
      {
        source: "/:path*",
        destination: "/pages/DynamicPage",
      },
    ];
  },
};

module.exports = nextConfig;

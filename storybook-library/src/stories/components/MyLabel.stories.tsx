import { Meta, StoryFn } from "@storybook/react";
import MyLabel from "../../components/MyLabel";

export default {
  title: "Licencias/Ejemplo",
  component: MyLabel,

  // Optional comboBox instead of radioButtons
  argTypes: {
    size: { control: "select", options: ["normal", "h1", "h2", "h3"] }, // Restricted
    color: { control: "select" },
    // backgroundColor: { control: 'color'} // Force colorPicker
  },
} as Meta<typeof MyLabel>;

const Template: StoryFn<typeof MyLabel> = (args) => <MyLabel {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  size: "normal",
  allCaps: false,
};
export const AllCaps = Template.bind({});
AllCaps.args = {
  size: "normal",
  allCaps: true,
};
export const Secondary = Template.bind({});
Secondary.args = {
  size: "normal",
  color: "secondary",
};
export const Tertiary = Template.bind({});
Tertiary.args = {
  size: "normal",
  color: "tertiary",
};

export const CustomFontColor = Template.bind({});
CustomFontColor.args = {
  size: "h1",
  color: "secondary",
  backgroundColor: "#444444",
};

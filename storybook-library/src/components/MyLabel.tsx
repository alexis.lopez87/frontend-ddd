import "./MyLabel.css";

export interface MyLabelProps {
  /**
   * Texto de etiqueta
   */
  label: string;
  /**
   * Tamaño de etiqueta
   */
  size: "normal" | "h1" | "h2" | "h3";
  /**
   * Todo en mayúsculas
   */
  allCaps?: boolean;
  /**
   * Colores básicos
   */
  color?: "primary" | "secondary" | "tertiary";
  /**
   * Colores texto fuente
   */
  backgroundColor?: string;
}

/**
 * Primary UI component for user interaction
 */
const MyLabel = ({
  allCaps = false,
  color = "primary",
  label = "Sin etiqueta",
  size = "normal",
  backgroundColor,
}: MyLabelProps) => {
  return (
    <span
      className={`label ${size} text-${color}`} //
      style={{ backgroundColor }}
    >
      {allCaps ? label.toUpperCase() : label}
    </span>
  );
};

export default MyLabel;

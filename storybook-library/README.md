# StoryBooks Components App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) And Storybook frontend workshop.

## Available Scripts

In the project directory, you can run:

### `npm start` or `npm storybook`

Runs the app in the development mode.\
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

### `npm build-storybook` or `npm build`

Export components to folder ~/storybook-static/, this folder can be seved as a standalone web, for example with http-server -i


### `npm serve`

 run the commands "http-server -o .\storybook-static" to serve the exported site, (you must have  http-server installed)
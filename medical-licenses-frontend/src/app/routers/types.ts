import { ComponentType, LazyExoticComponent } from "react";

export interface LocationStates {
  "/"?: {};
}
export type PathName = keyof LocationStates;

export interface Page {
  path: PathName;
  component: LazyExoticComponent<ComponentType<any>>;
  isPrivate: boolean;
}

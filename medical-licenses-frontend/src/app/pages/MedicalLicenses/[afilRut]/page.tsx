"use client";
import React, { useEffect, FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";

import { RootState } from "@/app/store";
import { MedicalLicensesRepository } from "@/infraestructure/repository/MedicalLicensesRepository";
import { GetPendingMedicalLicenses } from "@/domain/use-cases";
import { PendingMedicalLicensesResponse } from "@/domain/entities";
import { AgGridReact } from 'ag-grid-react';
import { ColDef } from 'ag-grid-community';
import { FORMAT_DATE, LIMIT } from "@/infraestructure/config/constants.config";

interface PageProps {
  params: { afilRut: number }
}

const Page: FC<PageProps> = ({ params }) => {
  const dispatch = useDispatch<ThunkDispatch<RootState, unknown, AnyAction>>();
  const medicalLicenses: PendingMedicalLicensesResponse = useSelector<RootState, PendingMedicalLicensesResponse>(
    (state: RootState) => state.medicalLicensesReducer
  );

  const columnDefs: ColDef[] = [
    { headerName: 'Nº licencia', field: 'medicalLicenseNumber', resizable: true },
    { headerName: 'Fecha inicio', field: 'startDate', resizable: true, cellRenderer: (data: any) => {
      return data.value ? (new Date(data.value)).toLocaleDateString(FORMAT_DATE) : '';
    } },
    { headerName: 'Fecha término', field: 'endDate', resizable: true, cellRenderer: (data: any) => {
      return data.value ? (new Date(data.value)).toLocaleDateString(FORMAT_DATE) : '';
    } },
    { headerName: 'Estado', field: 'status', resizable: true },
    {
      headerName: '',
      cellRenderer: () => (
        <button className="bg-blue-800 hover:bg-blue-900 text-white font-bold py-1 px-4 rounded h-10 my-1 flex items-center justify-center">
          Ver
        </button>
      ),
      cellStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      resizable: false,
    },
  ];

  useEffect(() => {
    const medicalLicensesRepository = new MedicalLicensesRepository();
    const pendingMedicalLicenses = new GetPendingMedicalLicenses(
      medicalLicensesRepository
    );
    dispatch(pendingMedicalLicenses.execute(params.afilRut, 1, LIMIT));
  }, [params.afilRut, dispatch]);

  useEffect(() => {
    var cssId = 'myCss';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = 'http://localhost:8080/main.css';
        link.media = 'all';
        head.appendChild(link);
    }
  })

  if (medicalLicenses.isLoading === true) {
    return (
      <div className="flex justify-center items-center h-20">
        <p className="text-black text-lg">Cargando...</p>
      </div>
    );
  }

  if (medicalLicenses.error) {
    console.error(medicalLicenses.error);
    return (
      <div className="flex justify-center items-center h-20">
        <p className="text-black text-lg">Ocurrio un error</p>
      </div>
    ); 
  }

  if (medicalLicenses.status === 'success' && medicalLicenses.data.length === 0) {
    return (
      <div className="flex justify-center items-center h-20">
        <p className="text-black text-lg">No se encontraron datos</p>
      </div>
    );
  }

  if (medicalLicenses.status === 'success' && medicalLicenses.data.length > 0) {
    return (
      <div className="w-full h-full max-w-screen-xl mx-auto">
        <h1 className="text-2xl font-bold text-blue-800 mb-4">Licencias Médicas Pendientes de Documentos</h1>
        <div
          id="myGrid"
          className="ag-theme-alpine"
        >
            <AgGridReact
              columnDefs={columnDefs}
              rowData={medicalLicenses.data}
              domLayout='autoHeight'
              rowHeight={50}
              onFirstDataRendered={(params) => {
                params.api.sizeColumnsToFit();
              }}
            />

        </div>
      </div>
    );
  }
};

export default Page;

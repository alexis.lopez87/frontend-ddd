import { PendingMedicalLicensesResponse } from "@/domain/entities";
import { MedicalLicensesTypes } from "@/domain/use-cases/types";

const initialState: PendingMedicalLicensesResponse = {
  total: 0,
  offset: 0,
  limit: 0,
  data: [],
  isLoading: false,
  error: undefined,
  status: "initial",
};

export const medicalLicensesReducer = (
  state = initialState,
  action: {
    type: MedicalLicensesTypes;
    payload?: PendingMedicalLicensesResponse;
  }
) => {
  const {
    GET_MEDICAL_LICENSES_SUCCESS,
    GET_MEDICAL_LICENSES_FAIL,
    GET_MEDICAL_LICENSES_REQUEST,
  } = MedicalLicensesTypes;

  switch (action.type) {
    case GET_MEDICAL_LICENSES_SUCCESS:
      return {
        ...state,
        ...action.payload,
        data: action.payload?.data || [],
        isLoading: false,
        status: "success",
      };
    case GET_MEDICAL_LICENSES_REQUEST:
      return {
        ...state,
        isLoading: true,
        status: "active",
      };
    case GET_MEDICAL_LICENSES_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.payload?.error,
        status: "error",
      };
    default:
      return state;
  }
};

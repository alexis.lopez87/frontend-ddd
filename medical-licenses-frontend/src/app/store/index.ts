"use client"
import { createContext } from "react";
import {combineReducers, compose} from "redux"
import { configureStore } from "@reduxjs/toolkit";
import thunkMiddleware from "redux-thunk";

import { medicalLicensesReducer } from "./medicalLicenses.reducer"

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const rootReducer = combineReducers({
  medicalLicensesReducer
})

const middleware = [thunkMiddleware];

export const store = configureStore({
  reducer: rootReducer,
  middleware: middleware,
  devTools: process.env.NODE_ENV !== "production",
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

export const StoreContext = createContext(store);

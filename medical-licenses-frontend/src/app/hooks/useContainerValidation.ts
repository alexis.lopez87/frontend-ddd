import { useEffect, useState } from "react";
import { CONTAINER_DOMAIN, ENVIRONMENT } from "../../infraestructure/config/constants.config";

const useContainerValidation = () => {
  const [allowRender, setAllowRender] = useState(false);

  useEffect(() => {
    const validateContainerURL = () => {
      const parentURL = document.referrer;

      if (parentURL !== CONTAINER_DOMAIN && ENVIRONMENT !== 'local') {
        console.log("La aplicación debe ejecutarse dentro del contenedor correcto.");
        document.body.innerHTML = "";
      } else {
        setAllowRender(true);
      }
    };

    validateContainerURL();
  }, [CONTAINER_DOMAIN]);

  useEffect(() => {
    const sendHeightToParent = () => {
      const height = document.body.scrollHeight;
      parent.postMessage({ type: "SET_HEIGHT", height }, "*");
    };

    sendHeightToParent();
  }, []);

  return allowRender;
};

export default useContainerValidation;

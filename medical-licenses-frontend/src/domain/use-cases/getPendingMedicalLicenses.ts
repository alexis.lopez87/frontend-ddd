import { PendingMedicalLicensesResponse } from "../entities";
import { IGetPendingMedicalLicenses } from "../ports/primaries";
import { IMedicalLicensesRepository } from "../ports/secondaries/IMedicalLicensesRepository";
import { MedicalLicensesTypes } from "./types";
import { Action, DispatchFn } from "./types/storeAdapter.types";

interface GetPendingMedicalLicensesAction
  extends Action<string, PendingMedicalLicensesResponse | unknown> {}

export class GetPendingMedicalLicenses implements IGetPendingMedicalLicenses {
  private _medicalLicensesRepository: IMedicalLicensesRepository;

  constructor(medicalLicensesRepository: IMedicalLicensesRepository) {
    this._medicalLicensesRepository = medicalLicensesRepository;
  }

  execute(
    afilRut?: number,
    page?: number,
    limit?: number
  ): (dispatch: DispatchFn<GetPendingMedicalLicensesAction>) => Promise<void> {
    const {
      GET_MEDICAL_LICENSES_SUCCESS,
      GET_MEDICAL_LICENSES_FAIL,
      GET_MEDICAL_LICENSES_REQUEST,
    } = MedicalLicensesTypes;
    return async (dispatch) => {
      try {
        dispatch({
          type: GET_MEDICAL_LICENSES_REQUEST,
          payload: {} as PendingMedicalLicensesResponse,
        });
        const pendingMedicalLicensesResponse: PendingMedicalLicensesResponse =
          await this._medicalLicensesRepository.getPendingMedicalLicenses(
            afilRut,
            page,
            limit
          );
        dispatch({
          type: GET_MEDICAL_LICENSES_SUCCESS,
          payload: pendingMedicalLicensesResponse,
        });
      } catch (error) {
        dispatch({
          type: GET_MEDICAL_LICENSES_FAIL,
          payload: error,
        });
      }
    };
  }
}

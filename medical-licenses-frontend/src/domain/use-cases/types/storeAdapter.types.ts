export interface DispatchFn<A> {
    (action: A): void;
  }
  
  export interface Action<T, P> {
    type: T;
    payload: P;
  }
  
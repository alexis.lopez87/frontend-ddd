export interface PendingMedicalLicenseResponse {
  medicalLicenseNumber: number;
  startDate: Date;
  endDate: Date;
  status: string;
}

export interface PendingMedicalLicensesResponse {
  total: number;
  offset: number;
  limit: number;
  data: PendingMedicalLicenseResponse[];
  isLoading: boolean;
  error?: Error;
  status: "success" | "error" | "active" | "initial";
}

import { PendingMedicalLicensesResponse } from "../../entities";

export interface IMedicalLicensesRepository {
    afilRut ?: number
    page?: number
    limit?: number
    getPendingMedicalLicenses(afilRut?: number, page?: number, limit?: number): Promise<PendingMedicalLicensesResponse>
}
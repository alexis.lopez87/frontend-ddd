export interface IGetPendingMedicalLicenses {
    execute(page?: number, results?: number): (dispatch: any) => Promise<void>
}
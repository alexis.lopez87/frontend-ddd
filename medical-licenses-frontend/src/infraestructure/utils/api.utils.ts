import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export enum RequestMethod {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
}

export class RestUtil {
  public async request<T, U>(
    method: RequestMethod,
    url: string,
    data?: T,
    headers?: any
  ) {
    try {
      const config: AxiosRequestConfig = {
        method,
        url,
        data,
        headers,
      };

      const response = await axios<U>(config);
      return response.data;
    } catch (error) {
      console.error("Error from API utils:", error);
      throw new Error("Error sending request to REST API");
    }
  }
}

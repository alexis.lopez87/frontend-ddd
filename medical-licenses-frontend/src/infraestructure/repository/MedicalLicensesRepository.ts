import {
  PendingMedicalLicenseResponse,
  PendingMedicalLicensesResponse,
} from "@/domain/entities";
import { IMedicalLicensesRepository } from "@/domain/ports/secondaries";
import {
  API_DOMAIN,
  IS_FAKE_BACKEND,
  API_DOMAIN_FAKE,
} from "../config/constants.config";
import { RequestMethod, RestUtil } from "../utils/api.utils";

export class MedicalLicensesRepository implements IMedicalLicensesRepository {
  private readonly restUtil = new RestUtil();

  async getPendingMedicalLicenses(
    afilRut: number,
    page = 1,
    limit = 10
  ): Promise<PendingMedicalLicensesResponse> {
    const method = RequestMethod.GET;

    const queryParams = {
      page: page.toString(),
      limit: limit.toString(),
    };

    let isFakeBackend: boolean = IS_FAKE_BACKEND === "true";
    let urlWithParams = "";

    if (!isFakeBackend) {
      urlWithParams = `${API_DOMAIN}/api/MedicalLicenses/affiliate/${afilRut}?${new URLSearchParams(
        queryParams
      )}`;
    } else {
      urlWithParams = `${API_DOMAIN_FAKE}/data`;
    }

    const response = await this.restUtil.request<
      undefined,
      PendingMedicalLicensesResponse
    >(method, urlWithParams);

    return response;
  }
}

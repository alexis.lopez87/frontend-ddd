export const API_DOMAIN =
  process.env.NEXT_PUBLIC_API_DOMAIN ?? "http://localhost:5001";
export const API_DOMAIN_FAKE =
  process.env.NEXT_PUBLIC_API_DOMAIN_FAKE ?? "http://localhost:3000";
export const FRONTEND_DOMAIN =
  process.env.NEXT_PUBLIC_FRONTEND_DOMAIN ?? "http://localhost:4201";
export const CONTAINER_DOMAIN =
  process.env.NEXT_PUBLIC_CONTAINER_DOMAIN ?? "http://localhost:4200/";
export const ENVIRONMENT = process.env.NEXT_PUBLIC_ENVIRONMENT ?? "local";
export const LIMIT = 10;
export const FORMAT_DATE = "en-GB";
export const IS_FAKE_BACKEND = process.env.NEXT_PUBLIC_IS_FAKE_BACKEND ?? "false";

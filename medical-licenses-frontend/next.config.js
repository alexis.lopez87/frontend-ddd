module.exports = {
    async headers() {
      return [
        {
          source: '/(.*)',
          headers: [
            // {
            //   key: 'Content-Security-Policy',
            //   value: "default-src 'self' http://localhost:8080; style-src 'self' 'unsafe-inline' http://localhost:8080",
            // },
            {
                key: 'Access-Control-Allow-Origin',
                value: '*',
            },
            {
              key: 'Access-Control-Allow-Methods',
              value: 'GET, POST, PUT, DELETE',
            },
            {
              key: 'Access-Control-Allow-Headers',
              value: 'Content-Type',
            },
          ],
        },
      ];
    },

    async rewrites() {
      return [
        {
          source: "/:afilRut",
          destination: "/pages/MedicalLicenses/:afilRut",
        },
      ];
    },
  };
  